# Desafio Captura

Este é um Crowler para a loja virtual da Época Cosméticos.

## Começando

### Pré-requisitos

* Docker
* Docker Compose

### Instalação

Execute o projeto através do docker-compose:

```shell
docker-compose -f ./docker-compose.yml up
```

Uma forma alternativa de executar o projeto é através do [make](https://en.wikipedia.org/wiki/Makefile):

```shell
make run
```

### Utilizando o Crawler

Envie um site para o Crawler através do seguinte endpoint:

```shell
curl –X POST \
-d 'url=http://www.epocacosmeticos.com.br/' \
http://localhost:5000/sites
```

Para baixar os produtos encontrados pelo crawler em formato CSV:
```shell
curl http://localhost:5000/products -o products.csv
```

## Testes unitários

Execute os testes unitários através do docker-compose:

```shell
docker-compose -f ./docker-compose.test.yml up
```

Uma forma alternativa de executar os testes unitários do projeto é através do [make](https://en.wikipedia.org/wiki/Makefile):

```shell
make test
```
