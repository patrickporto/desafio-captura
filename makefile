run:
	docker-compose -f ./docker-compose.yml build
	docker-compose -f ./docker-compose.yml up

test:
	docker-compose -f ./docker-compose.test.yml build
	docker-compose -f ./docker-compose.test.yml up

lint:
	pycodestyle .
