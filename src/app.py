from flask import Flask
from flask_restful import Api
from catalog.views import Sites, Products

app = Flask(__name__)
api = Api(app)

api.add_resource(Sites, '/sites')
api.add_resource(Products, '/products')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
