from contextlib import suppress
from bs4 import BeautifulSoup
from .models import Product
from requests.exceptions import MissingSchema, HTTPError
import requests


class Site(object):
    page_charset = 'utf-8'
    categories_selector = '.item_menu a'
    products_selector = 'li a.comprar'
    name_selector = '.name_brand .productName'

    @classmethod
    def _get_soup(cls, url):
        page_content = ''
        with suppress(MissingSchema, HTTPError):
            response = requests.get(url)
            response.raise_for_status()
            response.encoding = cls.page_charset
            page_content = response.text
        return BeautifulSoup(page_content, 'html.parser')

    @classmethod
    def get_categories(cls, url):
        soup = cls._get_soup(url)
        a_tags = soup.select(cls.categories_selector)
        links = [a.get('href') for a in a_tags]
        return set(links)

    @classmethod
    def get_products(cls, url):
        soup = cls._get_soup(url)
        a_tags = soup.select(cls.products_selector)
        links = [a.get('href') for a in a_tags]
        return set(links)

    @classmethod
    def get_product_detail(cls, url):
        soup = cls._get_soup(url)
        name = soup.select(cls.name_selector)[0].get_text(strip=True)
        title = soup.title.string
        return Product(name=name, title=title, url=url)

    @classmethod
    def has_product(cls, url):
        soup = cls._get_soup(url)
        return len(soup.select(cls.name_selector)) > 0
