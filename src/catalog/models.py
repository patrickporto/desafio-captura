from mongoengine import Document, StringField


class Product(Document):
    name = StringField(required=True)
    title = StringField(required=True)
    url = StringField(required=True)
