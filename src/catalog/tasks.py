from celery.utils.log import get_task_logger
from crawler import celery_app
from .epocacosmeticos import Site
from .models import Product


logger = get_task_logger(__name__)


@celery_app.task
def retrieve_page_product(url):
    if Site.has_product(url):
        product = Site.get_product_detail(url)
        if not Product.objects(url=product.url).first():
            logger.info('{0} has product {1}'.format(url, product.name))
            product.save()
    else:
        logger.info('URL {0} is not product'.format(url))
        retrieve_page_category.delay(url=url)


@celery_app.task
def retrieve_page_category(url):
    logger.info('Retrieving products of {0}'.format(url))
    for product_url in Site.get_products(url):
        logger.info('Found {0}'.format(product_url))
        retrieve_page_product.delay(url=product_url)


@celery_app.task
def retrieve_page(url):
    logger.info('Retrieving {0}'.format(url))
    for category_url in Site.get_categories(url):
        retrieve_page_category.delay(url=category_url)
