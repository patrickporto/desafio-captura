import pytest
import responses


@pytest.fixture
def home_page():
    body = """
    <nav class="menu100">
        <h2 class="item_menu perfumes">
            <a href="http://www.epocacosmeticos.com.br/perfumes" class="princ">Perfumes</a>
            <ul class="sub">
                <div class="col col1">
                    <h3>CATEGORIAS</h3>
                    <li><a href="http://www.epocacosmeticos.com.br/perfumes/perfume-feminino">Perfumes Femininos</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/perfumes/perfume-masculino">Perfumes Masculinos</a></li>
                    <li><a href="http://www.epocacosmeticos.com.br/perfumes/kits-de-perfumes">Kits de Perfumes</a></li>
                    <div class="botao-submenu">
                        <a href="http://www.epocacosmeticos.com.br/perfumes" class="sub_vertodos">+ PERFUMES</a>
                    </div>
                </div>
            </ul>
        </h2>
    </nav>
    """
    responses.add(responses.GET, 'http://www.epocacosmeticos.com.br/',
                  body=body, status=200)
    return body


@pytest.fixture
def category_page():
    body = """
    <div id="ResultItems_4494602" class="prat-home-2016">
        <div class="prat-home-2016 n4colunas">
            <ul>
            <li>
                <div class="desconto"><p><span>48%</span> off</p></div>
                    <input type="hidden" class="id" value="6164">
                    <input type="hidden" class="category" value="Perfume Feminino">
                    <a class="productImage" title="Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette" href="http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p"><img src="http://epocacosmeticos.vteximg.com.br/arquivos/ids/186007-178-174/very-irresistible-leau-en-rose-eau-de-toilette-givenchy-perfume-feminino-30ml.jpg" width="178" height="174" alt="Frasco do Perfume Feminino Very Irrésistible L'eau en Rose Eau de Toilette 30ml" id=""></a>
                    <div class="trustvox-widget-rating" data-trustvox-product-code="6164"><div class="ts-shelf-container" id="ts-js-shelf-6164">
                        <div>
                            <div class="ts-shelf-rate ts-sprite">
                            <div class="ts-shelf-rate-symbols ts-sprite" style="width: 86%;"></div>
                            </div>
                            <span class="ts-shelf-rate-count ts-shelf-rate-enabled">
                            (<span>125</span>)
                            </span>
                        </div>
                    </div>
                </div>
                <div class="discount"> </div>
                <p class="brand">
                    <a class="texto brand givenchy" href="http://www.epocacosmeticos.com.br/givenchy">Givenchy</a>
                </p>
                <h3>
                    <a title="Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette" href="http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p">
                        Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette
                    </a>
                </h3>
                <p class="price">
                <a title="Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette" href="http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p">
                    <span class="oldPrice"><span>R$ 249,00</span></span>
                    <span class="newPrice">R$ 129,00</span>
                    <em class="installment">
                        6X de R$ 21,50
                    </em>
                </a>
            </p>
            <a href="http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p" class="comprar">COMPRAR</a>
            <div class="frete">- com frete grátis -</div>
        </li>
        <li>
            <input type="hidden" class="id" value="4196">
            <input type="hidden" class="category" value="Perfume Feminino">
            <a class="productImage" title="La Vie Est Belle Lancôme - Perfume Feminino - Eau de Parfum" href="http://www.epocacosmeticos.com.br/la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino/p"><img src="http://epocacosmeticos.vteximg.com.br/arquivos/ids/217670-178-174/la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino-1.jpg" width="178" height="174" alt="la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino-1" id=""></a>
            <div class="trustvox-widget-rating" data-trustvox-product-code="4196"><div class="ts-shelf-container" id="ts-js-shelf-4196">
                <div>
                    <div class="ts-shelf-rate ts-sprite">
                        <div class="ts-shelf-rate-symbols ts-sprite" style="width: 96%;"></div>
                        </div>
                        <span class="ts-shelf-rate-count ts-shelf-rate-enabled">
                            (<span>589</span>)
                        </span>
                    </div>
                </div>
            </div>
            <div class="discount"> </div>
            <p class="brand"><a class="texto brand lancome" href="http://www.epocacosmeticos.com.br/lancome">Lancôme</a></p>
            <h3><a title="La Vie Est Belle Lancôme - Perfume Feminino - Eau de Parfum" href="http://www.epocacosmeticos.com.br/la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino/p">La Vie Est Belle Lancôme - Perfume Feminino - Eau de Parfum</a></h3>
            <p class="price">
                <a title="La Vie Est Belle Lancôme - Perfume Feminino - Eau de Parfum" href="http://www.epocacosmeticos.com.br/la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino/p">
                    <span class="oldPrice"></span>
                    <span class="newPrice">R$ 298,90</span>
                    <em class="installment">
                        10X de R$ 29,89
                    </em>
                </a>
            </p>
            <a href="http://www.epocacosmeticos.com.br/la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino/p" class="comprar">COMPRAR</a>
            <div class="frete">- com frete grátis -</div>
        </li>
        <li>
            <div class="desconto"><p><span>18%</span> off</p></div>
            <input type="hidden" class="id" value="14467">
            <input type="hidden" class="category" value="Perfume Feminino">
            <a class="productImage" title="212 NYC Seductive Body Spray Carolina Herrera - Perfume Feminino para o Corpo" href="http://www.epocacosmeticos.com.br/212-nyc-vintage-body-spray-carolina-herrera-perfume-masculino-para-o-corpo/p"><img src="http://epocacosmeticos.vteximg.com.br/arquivos/ids/215157-178-174/212-nyc-vintage-body-spray-carolina-herrera-perfume-feminino-para-o-corpo.jpg" width="178" height="174" alt="212-nyc-vintage-body-spray-carolina-herrera-perfume-feminino-para-o-corpo" id=""></a>
            <div class="trustvox-widget-rating" data-trustvox-product-code="14467">
                <div class="ts-shelf-container" id="ts-js-shelf-14467">
                    <div>
                        <div class="ts-shelf-rate ts-sprite">
                            <div class="ts-shelf-rate-symbols ts-sprite" style="width: 86%;"></div>
                        </div>
                        <span class="ts-shelf-rate-count ts-shelf-rate-enabled">
                            (<span>77</span>)
                        </span>
                    </div>
                </div>
            </div>
            <div class="discount"> </div>
            <p class="brand"><a class="texto brand carolina-herrera" href="http://www.epocacosmeticos.com.br/carolina-herrera">Carolina Herrera</a></p>
            <h3><a title="212 NYC Seductive Body Spray Carolina Herrera - Perfume Feminino para o Corpo" href="http://www.epocacosmeticos.com.br/212-nyc-vintage-body-spray-carolina-herrera-perfume-masculino-para-o-corpo/p">212 NYC Seductive Body Spray Carolina Herrera - Perfume Feminino para o Corpo</a></h3>
            <p class="price">
                <a title="212 NYC Seductive Body Spray Carolina Herrera - Perfume Feminino para o Corpo" href="http://www.epocacosmeticos.com.br/212-nyc-vintage-body-spray-carolina-herrera-perfume-masculino-para-o-corpo/p">
                    <span class="oldPrice"><span>R$ 159,00</span></span>
                    <span class="newPrice">R$ 129,00</span>
                    <em class="installment">
                        6X de R$ 21,50
                    </em>
                </a>
            </p>
            <a href="http://www.epocacosmeticos.com.br/212-nyc-vintage-body-spray-carolina-herrera-perfume-masculino-para-o-corpo/p" class="comprar">COMPRAR</a>
            <div class="frete">- com frete grátis -</div>
        </li>
        </ul>
    </div>
    <div class="btn-load-more confira-todos-produtos"><span>MOSTRAR MAIS PRODUTOS</span></div></div>
    """
    responses.add(responses.GET, 'http://www.epocacosmeticos.com.br/perfumes/perfume-feminino',
                  body=body, status=200)
    return body


@pytest.fixture
def product_page():
    body = """
    <head>
        <title>Perfume Very Irrésistible L`eau en Rose Givenchy Feminino - Época Cosméticos</title>
    </head>
    <body>
    <div class="dir com_sku">
        <section class="name_brand">
            <h1 itemprop="name">
                <div class="fn productName  Very-Irresistible-L`eau-en-Rose-Givenchy---Perfume-Feminino---Eau-de-Toilette ">
                    Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette - 30ml
                </div>
                <input id="___rc-p-id" type="hidden" value="6164">
                <input id="___rc-p-dv-id" type="hidden" value="129">
                <input id="___rc-p-sku-ids" type="hidden" value="10057,10059">
                <input id="___rc-p-kit-ids" type="hidden" value="">
            </h1>
            <div class="brands" itemprop="brand">
                <div class="brandName Givenchy ">
                    <a href="/givenchy" class="brand givenchy">Givenchy</a>
                </div>
            </div>
        </section>
        <div class="trustvox-link">
            <a id="trustvox-a" href="#reviews">
            <div class="trustvox-widget-rating" data-trustvox-product-code-js="skuJson.productId" data-trustvox-should-skip-filter="true" data-trustvox-display-rate-schema="true">
                <div class="ts-shelf-container" id="ts-js-shelf-6164">
                    <div itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                    <span class="ts-shelf-rate-value ts-shelf-rate-count ts-shelf-rate-enabled">
                        <span itemprop="ratingValue">4.3 </span> de <span itemprop="bestRating">5</span>
                    </span>
                    <div class="ts-shelf-rate ts-sprite">
                        <meta content="0" itemprop="worstRating">
                        <div class="ts-shelf-rate-symbols ts-sprite" style="width: 86%;"></div>
                    </div>
                    <span class="ts-shelf-rate-count ts-shelf-rate-enabled">
                    (<span itemprop="ratingCount">125</span>)
                    </span>
                </div>
            </div>
        </div>
        <span>veja as opiniões de quem já comprou</span></a>
    </div>
    <div class="avalia"></div>
    <div class="selectSku">
        <div class="load_sku" style="display: none;"></div>
        <div class="sku-selector-container sku-selector-container-0">
        <ul class="topic Variacao productid-6164 numopt-2">
            <li class="specification">Variação</li>
            <li class="select skuList item-dimension-Variacao">
                <span class="group_0">
                    <input type="radio" name="6164_Variacao" dimension="Variacao" data-value="30ml" data-dimension="Variação" class="skuselector-specification-label input-dimension-Variacao sku-selector skuespec_30ml change-image checked sku-picked" id="6164_Variacao_0" value="30ml" specification="30ml" checked="checked"><label for="6164_Variacao_0" class="dimension-Variacao espec_0 skuespec_30ml skuespec_Variação_opcao_30ml skuespec_Variacao_opcao_30ml checked sku-picked">30ml</label><input type="radio" name="6164_Variacao" dimension="Variacao" data-value="75ml" data-dimension="Variação" class="skuselector-specification-label input-dimension-Variacao sku-selector skuespec_75ml change-image" id="6164_Variacao_1" value="75ml" specification="75ml"><label for="6164_Variacao_1" class="dimension-Variacao espec_0 skuespec_75ml skuespec_Variação_opcao_75ml skuespec_Variacao_opcao_75ml">75ml</label></span></li></ul></div><script>$('.sku-selector-container-0').skuSelector(skuJson_0, {forceInputType: 'radio', selectSingleDimensionsOnOpening: 'true'});</script><!-- A viewpart skuRichSelection esta obsoleta. Use a viewpart skuSelection para renderizar no modo de exibicao configurado --></div><div class="cont_sku"><span class="arrow">
                </span>
                <div class="infoLeft"><h2 style="display: none;">30ml</h2>
                    <div class="refSku">
                        Ref: <div id="divStockKeepingUnitEan" class="sku-ean-wrapper">
                        <span id="divStockKeepingUnitEan" class="sku-ean-title">Código de Barras</span>
                        <label class="sku-ean-code">3274872276550</label>
                    </div>
                </div>
                <p class="priceOffProduto">48% OFF</p>
                <div class="preco"><div class="plugin-preco">
                    <div class="productPrice">
                        <p class="descricao-preco">
                        <em class="valor-de price-list-price" style="display: block;">
                            De <strong class="skuListPrice">R$ 249,00</strong></em><em class="valor-por price-best-price" style="display: block;">Por <strong class="skuBestPrice">R$ 129,00</strong>
                        </em>
                        <em class="valor-dividido price-installments" style="display: block;">
                            <span><span>ou <label class="skuBestInstallmentNumber">6<span class="x">x</span></label> de </span>
                            <strong><label class="skuBestInstallmentValue">R$ 21,50</label></strong></span>
                        </em>
                        </p>
                        <p class="preco-a-vista price-cash" style="display: block;">
                            Preço a vista:<strong class="skuPrice">R$ 129,00</strong>
                        </p>
                        <em class="economia-de">Economia de  <span class="economia">R$ 120,00</span></em>
                    </div>
                </div>
                <script>$('.plugin-preco').price(6164);</script>
            </div>
            <div style="display:none"></div>
        </div>
        <div class="comprar_new" style="opacity: 1;">
            <div style="display:none;" class="aguardecarregando">
                <img src="//epocacosmeticos.vteximg.com.br/arquivos/load.gif" alt="Carregando"></div>
                <a target="_top" class="buy-button buy-button-ref comprar-frete" href="/checkout/cart/add?sku=10057&amp;qty=1&amp;seller=1&amp;redirect=true&amp;sc=1" style="display:block">Comprar</a>
                <script>$('.buy-button-ref').buyButton(6164, {salesChannel: 1}, {})</script>
                <p class="unavailable-button" style="display:none">Produto Esgotado</p>
                <input type="hidden" class="buy-button-amount" value="1">
                <div class="portal-notify-me-ref">
                    <div class="notifyme sku-notifyme" style="display: none;"><div class="notifyme-title-div" style="display: none;">
                        <h3 class="notifymetitle notifyme-title">Produto esgotado no momento</h3>
                    </div>
                    <form action="/no-cache/AviseMe.aspx" style="display: none;">
                    <fieldset class="sku-notifyme-form notifyme-form">
                        <p>Não fique triste! Preencha os campos abaixo e nós te avisamos quando ele estiver de volta :)</p>
                        <input class="sku-notifyme-client-name notifyme-client-name" placeholder="Digite seu nome..." size="20" type="text" name="notifymeClientName" id="notifymeClientName" style="display: none;">
                        <input class="sku-notifyme-client-email notifyme-client-email" placeholder="Digite seu e-mail..." size="20" type="text" name="notifymeClientEmail" id="notifymeClientEmail" style="display: none;">
                        <input class="btn-ok sku-notifyme-button-ok notifyme-button-ok" value="ok" type="button" name="notifymeButtonOK" id="notifymeButtonOK" style="display: none;">
                        <input type="hidden" class="sku-notifyme-skuid notifyme-skuid" name="notifymeIdSku" value="" style="display: none;">
                    </fieldset>
                    </form>
                    <p class="notifyme-loading-message" style="display: none">
                        <span class="sku-notifyme-loading notifyme-loading">Carregando...</span>
                    </p>
                    <fieldset class="success" style="display:none;">
                        <label><em><span class="sku-notifyme-success notifyme-success">Cadastrado com sucesso, assim que o produto for disponibilizado você receberá um email avisando.</span></em></label>
                    </fieldset>
                    <fieldset class="error" style="display: none">
                        <label><span class="sku-notifyme-error   notifyme-error"></span></label>
                    </fieldset></div></div>
                    <script>var notifyMeOptions = {'strings': {"title":"Produto esgotado no momento","explanation":"\r\nNão fique triste! Preencha os campos abaixo e nós te avisamos quando ele estiver de volta :)\r\n  ","loading":"Carregando...","success":"Cadastrado com sucesso, assim que o produto for disponibilizado você receberá um email avisando.","error":"Não foi possível cadastrar. Tente mais tarde.","emailErrorMessage":"O endereço de e-mail informado é inválido."}};
                        $('.portal-notify-me-ref').notifyMe(6164, notifyMeOptions);
                    </script>
                </div>
            </div>
            <div class="cont_infos">
                <div class="util">
                    <ul>
                    <li class="prazo">
                        <p id="popupCalculoFreteWrapper" class="frete">
                            <a href="javascript:void(0);" title="Calcule o valor do frete e prazo de entrega para a sua região" class="shipping-value">Calcule o valor do frete e prazo de entrega para a sua região</a>
                        </p>
                        <div id="calculoFrete" style="display:none;">
                            <div class="contentWrapper">
                                <div class="header">
                                    <h2>Insira o CEP para calcular o prazo de entrega</h2>
                                    <div class="close">
                                        <a href="javascript:void(0);" class="bt btn-thickbox" title="Fechar" id="lnkFechar1">Fechar</a>
                                    </div>
                                </div>
                                <div id="ctl00_Conteudo_upnlContent">
                                    <div class="content">
                                        <fieldset>
                                            <label class="prefixo">
                                                Insira o CEP para calcular o prazo de entrega
                                                <input type="tel" class="fitext freight-zip-box" id="txtCep" name="txtCep" maxlength="8" value="">
                                            </label>
                                            <span class="frete-calcular btBordas btBordasInput">
                                                <input type="button" class="bt freight-btn" title="OK" id="btnFreteSimulacao" value="OK" name="btnFreteSimulacao">
                                            </span>
                                            <span class="cep-busca"><a title="Não sei meu CEP" class="bt lnkExterno" target="_blank" href="http://www.buscacep.correios.com.br/sistemas/buscacep/">Não sei meu CEP</a></span>
                                        </fieldset>
                                        <div class="freight-values" style="display: none"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="seguro">Site seguro</li>
                </ul>
            </div>
            <div class="banner-produto"></div>
                <ul class="tab">
                    <li class="descricao ativo">
                        <h2><a href="#">Descrição</a></h2>
                    </li>
                </ul>
                <div class="cont_tab"><div class="descr"><div class="productDescription">
                    <strong>Very Irrésistible</strong> é uma criação única que reflete uma amizade que marcou profundamente a história da casa: a de audrey hepburn e hubert de givenchy. A fragrância incorpora toda a sofisticação deste par singular. Tornando-se um ótimo clássico entre as fragrâncias da marca, e agora comemora dez anos de sucesso.<br><br><strong>Very Irrésistible L`eau en Rose</strong> é uma fragrância Eau de Toilette de <strong>Givenchy</strong>. <br><br>Muito Irresistível, muito você ! <br><br><strong>L'Eau en Rose</strong> tem essência de rosas, que há muito tempo são reverenciadas como o símbolo supremo da feminilidade e da elegância, são o coração e a alma do perfume clássico, Givenchy Very Irrésistible. <br><br>Etéreo e emblemático, <strong>L'Eau en Rose</strong> comemora o décimo ano de "Very Irrésistible" e a rosa com um toque gelado e floral sobre esse aroma icônico. <br><br>Impossível resistir, <strong>L'Eau en Rose</strong> evoca a energia fresca e o encanto desarmante da mulher irresistível. <br><br>Sendo um clássico floral, a fragrância se centra em torno do suave arrepio de rosa fosca e amora preta misturada com notas de musk macias e ondulantes que melhoram sua trilha sensual. <br><br><strong>L'Eau en Rose</strong> é tudo de uma vez, fresco, atraente e inerentemente feminino.</div><div id="caracteristicas"><h4 class="group Descricao-Detalhada">Descrição Detalhada</h4><table cellspacing="0" class="group Descricao-Detalhada"><tbody><tr class="even"><th class="name-field Video" valign="top">Video</th><td class="value-field Video">//www.youtube.com/embed/zK2nX_DrzA4</td></tr></tbody></table><h4 class="group Concentracao-">Concentração:</h4><table cellspacing="0" class="group Concentracao-"><tbody><tr><th class="name-field Concentracao-" valign="top">Concentração:</th><td class="value-field Concentracao-">Eau de Toilette - EDT</td></tr></tbody></table><h4 class="group Familia-Olfativa-">Familia Olfativa:</h4><table cellspacing="0" class="group Familia-Olfativa-"><tbody><tr class="even"><th class="name-field Familia-Olfativa-" valign="top">Familia Olfativa:</th><td class="value-field Familia-Olfativa-">Floral</td></tr></tbody></table><h4 class="group Notas-de-Topo-">Notas de Topo:</h4><table cellspacing="0" class="group Notas-de-Topo-"><tbody><tr><th class="name-field Notas-de-Topo-" valign="top">Notas de Topo:</th><td class="value-field Notas-de-Topo-">Amora</td></tr></tbody></table><h4 class="group Notas-de-Coracao-">Notas de Coração:</h4><table cellspacing="0" class="group Notas-de-Coracao-"><tbody><tr class="even"><th class="name-field Notas-de-Coracao-" valign="top">Notas de Coração:</th><td class="value-field Notas-de-Coracao-">Rosa</td></tr></tbody></table><h4 class="group Notas-de-Fundo-">Notas de Fundo:</h4><table cellspacing="0" class="group Notas-de-Fundo-"><tbody><tr><th class="name-field Notas-de-Fundo-" valign="top">Notas de Fundo:</th><td class="value-field Notas-de-Fundo-">Almíscar</td></tr></tbody></table><h4 class="group Ocasiao-">Ocasião:</h4><table cellspacing="0" class="group Ocasiao-"><tbody><tr class="even"><th class="name-field Ocasiao" valign="top">Ocasião</th><td class="value-field Ocasiao">Sofisticada</td></tr></tbody></table><h4 class="group Apresentacao">Apresentação</h4><table cellspacing="0" class="group Apresentacao"><tbody><tr><th class="name-field Apresentacao" valign="top">Apresentação</th><td class="value-field Apresentacao">30ml, 50ml, 75ml</td></tr></tbody></table><h4 class="group Perfumista">Perfumista</h4><table cellspacing="0" class="group Perfumista"><tbody><tr class="even"><th class="name-field Perfumista" valign="top">Perfumista</th><td class="value-field Perfumista">O perfumista que assina esta fragrância é Carlos Benaim</td></tr></tbody></table><h4 class="group Conheca-a-Linha-">Conheça a Linha:</h4><table cellspacing="0" class="group Conheca-a-Linha-"><tbody><tr><th class="name-field Conheca-a-Linha-" valign="top">Conheça a Linha:</th><td class="value-field Conheca-a-Linha-"><a href="http://www.epocacosmeticos.com.br/givenchy " title=" Perfumes Givenchy">Conheça os perfumes Givenchy na Época Cosméticos</a></td></tr></tbody></table><h4 class="group Sobre-a-Marca-">Sobre a Marca:</h4><table cellspacing="0" class="group Sobre-a-Marca-"><tbody><tr class="even"><th class="name-field Sobre-a-Marca-" valign="top">Sobre a Marca:</th><td class="value-field Sobre-a-Marca-"><a href="http://www.epocacosmeticos.com.br/givenchy " title=" Perfumes Givenchy"><img src="http://www.epocacosmeticos.com.br/arquivos/Givenchy_carrossel.jpg" align="left" alt="Perfumes Givenchy" style="margin-right:10px"></a> Hubert de Givenchy é uma figura legendária. Em 1952, ele abria sua maison em Paris, revolucionando a alta costura. O primeiro designer a apresentar uma coleção prêt-à-porter feminina lançou em 1957 seu primeiro perfume,"Le De". Já no século 21, Liv Tyler é o rosto de "Very Irrésistible Givenchy", um mix da elegância francesa e espontaneidade americana, presente em todas as coleções do estilista.
                    <br><a href="http://www.epocacosmeticos.com.br/givenchy " title=" Perfumes Givenchy">Conheça os produtos Givenchy na Época Cosméticos.</a></td></tr></tbody></table><h4 class="group Garantia-">Garantia:</h4><table cellspacing="0" class="group Garantia-"><tbody><tr><th class="name-field Garantia-" valign="top">Garantia:</th><td class="value-field Garantia-">Todos os nossos produtos são 100% originais e distribuído por fornecedores oficiais de cada marca no Brasil.
                    <br><a href="/centralatendimento/vantagens-qualidade-confianca">Saiba mais sobre porque é confiável comprar na Época Cosméticos.</a></td></tr></tbody></table></div></div><div class="dicas_aplicacao" style="display:none;"><p>conteúdo abaixo da descrição - inativo. </p>
                </div>
            </div>
        </div>
    </div>
    </body>
    """
    responses.add(responses.GET, 'http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p',
                  body=body, status=200)
    return body
