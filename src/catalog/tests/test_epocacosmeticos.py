import responses
from ..epocacosmeticos import Site


@responses.activate
def test_site_get_categories(home_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/'
    expected_categories = [
        'http://www.epocacosmeticos.com.br/perfumes/perfume-feminino',
        'http://www.epocacosmeticos.com.br/perfumes/perfume-masculino',
        'http://www.epocacosmeticos.com.br/perfumes/kits-de-perfumes',
        'http://www.epocacosmeticos.com.br/perfumes',
    ]
    # act
    categories = Site.get_categories(url)
    # assert
    assert sorted(expected_categories) == sorted(categories)


@responses.activate
def test_site_get_product_list(category_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/perfumes/perfume-feminino'
    expected_products = [
        'http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p',
        'http://www.epocacosmeticos.com.br/la-vie-est-belle-eau-de-parfum-lancome-perfume-feminino/p',
        'http://www.epocacosmeticos.com.br/212-nyc-vintage-body-spray-carolina-herrera-perfume-masculino-para-o-corpo/p',
    ]
    # act
    products = Site.get_products(url)
    # assert
    assert sorted(expected_products) == sorted(products)


@responses.activate
def test_site_get_product_detail(product_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p'
    # act
    product = Site.get_product_detail(url)
    # assert
    assert product.name == 'Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette - 30ml'
    assert product.title == 'Perfume Very Irrésistible L`eau en Rose Givenchy Feminino - Época Cosméticos'
    assert product.url == url


@responses.activate
def test_has_product_when_page_category(category_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/perfumes/perfume-feminino'
    # act
    has_product = Site.has_product(url)
    # assert
    assert has_product is False


@responses.activate
def test_has_product_when_page_product(product_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p'
    # act
    has_product = Site.has_product(url)
    # assert
    assert has_product is True
