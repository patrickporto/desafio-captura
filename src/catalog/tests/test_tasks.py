from unittest.mock import patch, call
import sys
import pytest
import responses
from ..models import Product
from ..tasks import retrieve_page, retrieve_page_product


@patch('catalog.tasks.retrieve_page_category.delay')
@responses.activate
def test_retrieve_page(retrieve_page_category, home_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/'
    # act
    retrieve_page(url=url)
    # assert
    retrieve_page_category.assert_has_calls([
        call(url='http://www.epocacosmeticos.com.br/perfumes/perfume-feminino'),
        call(url='http://www.epocacosmeticos.com.br/perfumes/perfume-masculino'),
        call(url='http://www.epocacosmeticos.com.br/perfumes/kits-de-perfumes'),
        call(url='http://www.epocacosmeticos.com.br/perfumes'),
    ], any_order=True)


@responses.activate
def test_retrieve_page_product_when_product(product_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p'
    sys.setrecursionlimit(sys.getrecursionlimit() + 100)
    # act
    retrieve_page_product(url=url)
    # assert
    products = [p for p in Product.objects.all()]
    assert len(products) == 1
    assert products[0].name == 'Very Irrésistible L`eau en Rose Givenchy - Perfume Feminino - Eau de Toilette - 30ml'
    assert products[0].title == 'Perfume Very Irrésistible L`eau en Rose Givenchy Feminino - Época Cosméticos'
    assert products[0].url == 'http://www.epocacosmeticos.com.br/very-irresistible-l-eau-en-rose-eau-de-toilette-givenchy-perfume-feminino/p'


@patch('catalog.tasks.retrieve_page_category.delay')
@responses.activate
def test_retrieve_page_product_when_category(retrieve_page_category, category_page):
    # arrange
    url = 'http://www.epocacosmeticos.com.br/perfumes/perfume-feminino'
    # act
    retrieve_page_product(url=url)
    # assert
    retrieve_page_category.assert_called_once_with(url='http://www.epocacosmeticos.com.br/perfumes/perfume-feminino')
