from flask import Response
from flask_restful import Resource
from webargs import fields
from webargs.flaskparser import use_kwargs, parser
from catalog.tasks import retrieve_page
from catalog.models import Product


class Sites(Resource):
    args = {
        'url': fields.Str(
            required=True,
        ),
    }

    @use_kwargs(args)
    def post(self, url):
        retrieve_page.delay(url=url)
        return ('', 204)


class Products(Resource):
    @staticmethod
    def _generate_products():
        for product in Product.objects():
            yield ';'.join([product.name, product.title, product.url]) + '\n'

    def get(self):
        # Streaming Contents
        return Response(
            self._generate_products(),
            mimetype='text/csv',
            headers={
                'Content-Disposition': 'attachment; filename=products.csv'
            },
        )
