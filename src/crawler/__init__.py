from mongoengine import connect
from . import settings
from .celery import celery_app

__all__ = ['celery_app']

connect(host=settings.DATABASE_URL)
