from celery import Celery
from crawler import settings


celery_app = Celery('tasks', broker=settings.BROKER_URL)
celery_app.conf.imports = ['catalog.tasks']
