from decouple import config

BROKER_URL = config('CELERY_BROKER_URL', 'amqp://')
DATABASE_URL = config('DATABASE_URL')
